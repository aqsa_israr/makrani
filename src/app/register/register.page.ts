import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  email: string;
  password: string;
  cpassword: string;
  errors: string[];
  constructor(public afAuth: AngularFireAuth, 
    public router: Router,
    public afStore: AngularFirestore,
    private storage: Storage) { 
    this.errors = [];
  }

  ngOnInit() {
  }

  async registerUser(){
    this.errors = [];
    const {email, password, cpassword} = this;
    if(password !== cpassword){
      this.errors.push("passwords don't match");
      return;
    }

    try {
      const res = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
      const username = email;
      if(res.user){
        this.storage.set('user', {
          username: email,
          uid: res.user.uid
        }).then((response) => {
          this.router.navigate(['profile']);
        }).catch((error) => {
        });
      }
    } catch (error) {
      console.dir(error);
      this.errors.push(error.message);
    }
  }

}
