import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  searchResults: any[];
  searchText: string;
  constructor(public afStore: AngularFirestore) { }

  ngOnInit() {
    this.searchResults=[]; 
    this.searchText="";
  }

  performSearch(){
    this.searchResults=[]; 
    this.afStore.collection('users', ref => ref
    .orderBy('sFirstName')
    .startAt(this.searchText.toLowerCase())
    .endAt(this.searchText.toLowerCase()+"\uf8ff"))
    .valueChanges().subscribe((results) => {
      console.log(results);
      results.forEach(element => {
        this.searchResults.push(element);
      });
      
    });

    this.afStore.collection('users', ref => ref
    .orderBy('sLastName')
    .startAt(this.searchText.toLowerCase())
    .endAt(this.searchText.toLowerCase()+"\uf8ff"))
    .valueChanges().subscribe((results) => {
      console.log(results);
      results.forEach(element => {
        this.searchResults.push(element);
      });
      
    });

    this.afStore.collection('users', ref => ref
    .orderBy('sBirthPlace')
    .startAt(this.searchText.toLowerCase())
    .endAt(this.searchText.toLowerCase()+"\uf8ff"))
    .valueChanges().subscribe((results) => {
      console.log(results);
      results.forEach(element => {
        this.searchResults.push(element);
      });
    });
  }

}
