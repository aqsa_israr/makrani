import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

interface spouse {
  fullName: string,
  dob: string,
  qualificationReg: string,
  qualificationAcd: string,
  occupation: string
}


@Component({
  selector: 'app-spouse',
  templateUrl: './spouse.page.html',
  styleUrls: ['./spouse.page.scss'],
})
export class SpousePage implements OnInit {
  spouse: spouse;
  constructor(public afStore: AngularFirestore,
    public user: UserService,
    public router: Router,
    private storage: Storage) { 
      this.spouse = {
        fullName: null,
        dob: null,
        qualificationReg: null,
        qualificationAcd: null,
        occupation: null
      }
    }

  ngOnInit() {
    var formData = this.user.getSurveyData();
    if(!formData.spouse){
      this.spouse = {
        fullName: null,
        dob: null,
        qualificationReg: null,
        qualificationAcd: null,
        occupation: null
      }
    }
    else{
      this.spouse = formData.spouse;
    }
  }

  postData(){
    const {spouse} = this;
    this.storage.get('user').then((val) => {
      if(val){
        this.afStore.doc(`users/${val.uid}`).update({
          spouse: spouse
        });
        this.router.navigate(['members']);
      }
    }).catch((error) => {
    });
    
  }

}
