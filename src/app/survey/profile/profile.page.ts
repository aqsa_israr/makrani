import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  personalInfo: {
    firstName: string,
    lastName: string,
    gender: string,
    maritalStatus: string,
    dob: string,
    birthPlace: string,
    mobileNumber: string,
    email: string,
    house: string,
    address: string,
    hobbies: string,
    achievement: string,
    qualificationReg: string,
    qualification: string,
    disablity: string,
    familyMembers: number,
    mmFamilyMembers: number,
    mfFamilyMembers: number,
    umFamilyMembers: number,
    ufFamilyMembers: number,
    profDetails: string,
    officeAddress: string,
    officeNum: string,
    officeName: string,
    department: string,
    designation: string,
    income: string
  };
  errors: string[];
  serviceInitialized: boolean;
  
  constructor(public afStore: AngularFirestore,
    public user: UserService,
    public router: Router,
    private storage: Storage) { 
    this.errors = [];
    this.initializeSurveyForm();
  }

  ngOnInit() {
    this.initializeData();
    
  }

  initializeSurveyForm(){
    this.personalInfo = {
      firstName: null,
      lastName: null,
      gender: null,
      maritalStatus: null,
      dob: null,
      birthPlace: null,
      mobileNumber: null,
      email: null,
      house: null,
      address: null,
      hobbies: null,
      achievement: null,
      qualificationReg: null,
      qualification: null,
      disablity: null,
      familyMembers: null,
      mmFamilyMembers: null,
      mfFamilyMembers: null,
      umFamilyMembers: null,
      ufFamilyMembers: null,
      profDetails: null,
      officeAddress: null,
      officeNum: null,
      officeName: null,
      department: null,
      designation: null,
      income: null
    };
  }

  postData(){
    const {personalInfo} = this;
    var sFirstName = null;
    var sLastName = null;
    var sBirthPlace = null;
    this.storage.get('user').then((val) => {
      if(val){
        if(personalInfo.firstName){
          sFirstName = personalInfo.firstName.toLowerCase();
        }
        if(personalInfo.lastName){
          sLastName = personalInfo.lastName.toLowerCase();
        }
        if(personalInfo.birthPlace){
          sBirthPlace = personalInfo.birthPlace.toLowerCase();
        }
        
        this.afStore.doc(`users/${val.uid}`).update(personalInfo).then((responce) => {
          if(!this.serviceInitialized){
            this.initializeService();
          }
          this.afStore.doc(`users/${val.uid}`).update({sFirstName,
            sLastName, sBirthPlace}).then((responce)=>{
              this.router.navigate(['spouse']);
            });
        });
        
      }
    }).catch((error) => {
    });
  }

  initializeData(){
    this.storage.get('user').then((val) => {
      if(val){
        this.afStore.doc(`users/${val.uid}`).get().subscribe(doc => {
          if (doc.exists) {
            this.personalInfo = {
              firstName: doc.data()['firstName'],
              lastName: doc.data()['lastName'],
              gender: doc.data()['gender'],
              maritalStatus: doc.data()['maritalStatus'],
              dob: doc.data()['dob'],
              birthPlace: doc.data()['birthPlace'],
              mobileNumber: doc.data()['mobileNumber'],
              email: doc.data()['email'],
              house: doc.data()['house'],
              address: doc.data()['address'],
              hobbies: doc.data()['hobbies'],
              achievement: doc.data()['achievement'],
              qualificationReg: doc.data()['qualificationReg'],
              qualification: doc.data()['qualification'],
              disablity: doc.data()['disablity'],
              familyMembers: doc.data()['familyMembers'],
              mmFamilyMembers: doc.data()['mmFamilyMembers'],
              mfFamilyMembers: doc.data()['mfFamilyMembers'],
              umFamilyMembers: doc.data()['umFamilyMembers'],
              ufFamilyMembers: doc.data()['ufFamilyMembers'],
              profDetails: doc.data()['profDetails'],
              officeAddress: doc.data()['officeAddress'],
              officeNum: doc.data()['officeNum'],
              officeName: doc.data()['officeName'],
              department: doc.data()['department'],
              designation: doc.data()['designation'],
              income: doc.data()['income']
            };
    
            var serviceData = {
              firstName: doc.data()['firstName'],
              lastName: doc.data()['lastName'],
              gender: doc.data()['gender'],
              maritalStatus: doc.data()['maritalStatus'],
              dob: doc.data()['dob'],
              birthPlace: doc.data()['birthPlace'],
              mobileNumber: doc.data()['mobileNumber'],
              email: doc.data()['email'],
              house: doc.data()['house'],
              address: doc.data()['address'],
              hobbies: doc.data()['hobbies'],
              achievement: doc.data()['achievement'],
              qualificationReg: doc.data()['qualificationReg'],
              qualification: doc.data()['qualification'],
              disablity: doc.data()['disablity'],
              familyMembers: doc.data()['familyMembers'],
              mmFamilyMembers: doc.data()['mmFamilyMembers'],
              mfFamilyMembers: doc.data()['mfFamilyMembers'],
              umFamilyMembers: doc.data()['umFamilyMembers'],
              ufFamilyMembers: doc.data()['ufFamilyMembers'],
              profDetails: doc.data()['profDetails'],
              officeAddress: doc.data()['officeAddress'],
              officeNum: doc.data()['officeNum'],
              officeName: doc.data()['officeName'],
              department: doc.data()['department'],
              designation: doc.data()['designation'],
              income: doc.data()['income'],
              members: doc.data()['members'],
              spouse: doc.data()['spouse']
            };
    
            this.user.setSurveyData(serviceData);
            this.serviceInitialized = true;
          }
          else{
            var username = val.username;
            this.afStore.doc(`users/${val.uid}`).set({
              username
            });
            this.serviceInitialized = false;
          }
        });
      }
    }).catch((error) => {
    });
  }

  initializeService(){
    var serviceData = {
      firstName: this.personalInfo['firstName'],
      lastName: this.personalInfo['lastName'],
      gender: this.personalInfo['gender'],
      maritalStatus: this.personalInfo['maritalStatus'],
      dob: this.personalInfo['dob'],
      birthPlace: this.personalInfo['birthPlace'],
      mobileNumber: this.personalInfo['mobileNumber'],
      email: this.personalInfo['email'],
      house: this.personalInfo['house'],
      address: this.personalInfo['address'],
      hobbies: this.personalInfo['hobbies'],
      achievement: this.personalInfo['achievement'],
      qualificationReg: this.personalInfo['qualificationReg'],
      qualification: this.personalInfo['qualification'],
      disablity: this.personalInfo['disablity'],
      familyMembers: this.personalInfo['familyMembers'],
      mmFamilyMembers: this.personalInfo['mmFamilyMembers'],
      mfFamilyMembers: this.personalInfo['mfFamilyMembers'],
      umFamilyMembers: this.personalInfo['umFamilyMembers'],
      ufFamilyMembers: this.personalInfo['ufFamilyMembers'],
      profDetails: this.personalInfo['profDetails'],
      officeAddress: this.personalInfo['officeAddress'],
      officeNum: this.personalInfo['officeNum'],
      officeName: this.personalInfo['officeName'],
      department: this.personalInfo['department'],
      designation: this.personalInfo['designation'],
      income: this.personalInfo['income'],
      members: undefined,
      spouse: undefined
    };

    this.user.setSurveyData(serviceData);
  }

}
