import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase';
import { UserService } from 'src/app/user.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { NavController } from '@ionic/angular';

interface member {
  type: string,
  fullName: string,
  maritalStatus: string,
  address: string,
  qualificationReg: string,
  qualificationAcd: string,
  occupation: string,
  officeAddress: string,
  designation: string,
  salary: string
}

@Component({
  selector: 'app-members',
  templateUrl: './members.page.html',
  styleUrls: ['./members.page.scss'],
})

export class MembersPage implements OnInit {
  errors: string[];
  memberCount: number;
  membersArray: member[];

  constructor(public afStore: AngularFirestore,
    public user: UserService,
    private storage: Storage,
    public router: Router,
    private toastCtrl: ToastController,
    public nav: NavController) { 
    this.errors = [];
    this.membersArray = [];
    this.memberCount = 6;
  }

  ngOnInit() {
    var formData = this.getData();
    if(!formData.members){
      for(var i=0; i< this.memberCount; i++){
        let member = {
          type: null,
          fullName: null,
          maritalStatus: null,
          address: null,
          qualificationReg: null,
          qualificationAcd: null,
          occupation: null,
          officeAddress: null,
          designation: null,
          salary: null
        };
        this.membersArray.push(member);
      }
    }
    else{
      this.membersArray = formData.members;
      if(this.membersArray.length < 6){
        const remainingMembers = this.memberCount - this.membersArray.length;
        for(var i=0; i< remainingMembers; i++){
          let member = {
            type: null,
            fullName: null,
            maritalStatus: null,
            address: null,
            qualificationReg: null,
            qualificationAcd: null,
            occupation: null,
            officeAddress: null,
            designation: null,
            salary: null
          };
          this.membersArray.push(member);
        }
      }
    }
     
  }

  postData(){
    this.storage.get('user').then((val) => {
      if(val){
        this.afStore.doc(`users/${val.uid}`).update({
          members: firestore.FieldValue.delete()
       });
       this.membersArray.forEach(element => {
         this.afStore.doc(`users/${val.uid}`).update({
           members: firestore.FieldValue.arrayUnion(element)
         });
       });
      let toast = this.toastCtrl.create({
          message: 'Congratulations! Your Survey is complete. You can update your survey anytime',
          showCloseButton: true,
          position: "bottom"
        }).then((toastData)=>{
          toastData.present();
          this.storage.clear().then((val) => {
          this.user.setSurveyData(null);
          this.nav.navigateRoot('login');
          }).catch((error) => {
          });
        });
      }
    }).catch((error) => {
    });
    

  }

  getData(){
    return this.user.getSurveyData();
  }

}
