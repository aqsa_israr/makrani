import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { MenuController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    public router: Router,
    public user: UserService,
    public menuCtrl: MenuController,
    public nav: NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logout(){
    this.storage.clear().then((val) => {
      this.user.setSurveyData(null);
      this.nav.navigateRoot('login');
      this.menuCtrl.toggle();
    }).catch((error) => {
    });
  }

  search(){
    this.router.navigate(['search']);
    this.menuCtrl.toggle();
  }
}
