import { Injectable } from '@angular/core';

interface user {
    username: string,
    uid: string
}

interface spouse {
    fullName: string,
    dob: string,
    qualificationReg: string,
    qualificationAcd: string,
    occupation: string
}

interface member {
    type: string,
    fullName: string,
    maritalStatus: string,
    address: string,
    qualificationReg: string,
    qualificationAcd: string,
    occupation: string,
    officeAddress: string,
    designation: string,
    salary: string
}

interface personalInfo {
    firstName: string,
    lastName: string,
    gender: string,
    maritalStatus: string,
    dob: string,
    birthPlace: string,
    mobileNumber: string,
    email: string,
    house: string,
    address: string,
    hobbies: string,
    achievement: string,
    qualificationReg: string,
    qualification: string,
    disablity: string,
    familyMembers: number,
    mmFamilyMembers: number,
    mfFamilyMembers: number,
    umFamilyMembers: number,
    ufFamilyMembers: number,
    profDetails: string,
    officeAddress: string,
    officeNum: string,
    officeName: string,
    department: string,
    designation: string,
    income: string,
    members: member[],
    spouse: spouse
  };

@Injectable()
export class UserService{
    private user: user;
    private personalInfo: personalInfo;
    constructor(){

    }

    setSurveyData(personalInfo: personalInfo){
        this.personalInfo = personalInfo;
    }
    getSurveyData(){
        return this.personalInfo;
    }
}