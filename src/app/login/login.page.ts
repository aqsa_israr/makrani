import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { ToastController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password: string;
  errors: string[];
  constructor(public afAuth: AngularFireAuth, 
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public router: Router,
    private storage: Storage) { 
    this.errors = [];
  }

  ngOnInit() {
    this.storage.get('user').then((val) => {
      if(val){
        this.router.navigate(['profile']);
      }
    }).catch((error) => {
    });
  }

  async login(){
    this.errors = [];
    const {email, password} = this;
    try {
      const res = await this.afAuth.auth.signInWithEmailAndPassword(email, password);
      if(res.user){
        this.storage.set('user', {
          username: email,
          uid: res.user.uid
        }).then((response) => {
          this.router.navigate(['profile']);
        }).catch((error) => {
        });
      }
    } catch (error) {
      console.dir(error);
      if(error.code === 'auth/user-not-found'){
        this.errors.push('User Not Found.');
      }
      else{
        this.errors.push(error.message);
      }
    }
  }

  resetpassword() {
		if (!this.email || !this.validateEmail(this.email)) {
			this.presentToast("Please enter a valid email to reset password");
		}
		else {
			let requestLoader = this.loadingCtrl.create({
        message: "Requesting...",
        duration: 20000
			}).then((requestLoader)=>{
        requestLoader.present();
        this.afAuth.auth.signInWithEmailAndPassword(this.email, 'nnnn')
        .then((obj) => {},
					error => {
						if(error.code === 'auth/user-not-found' || error.code === 'auth/invalid-email'){
							requestLoader.dismiss();
							this.presentToast("No user with such email exist");
						}
						else if(error.code === 'auth/wrong-password'){
							this.afAuth.auth.sendPasswordResetEmail(this.email)
							.then(
								() => {
									requestLoader.dismiss();
									this.presentToast("A link has been sent to your email");
								},
								error => {
									requestLoader.dismiss();
									this.presentToast(error.message);
								}
							)
						}
					}
				);
      });
		}
  }
  
  presentToast(message) {
    let toast = this.toastCtrl.create({
        message: message,
        showCloseButton: true,
			  position: "bottom"
      }).then((toastData)=>{
        console.log(toastData);
        toastData.present();
      });
  }

  validateEmail(email) : boolean {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.match(mailformat)) {
      return true;
    }
    else {
      return false;
    }
  }

}
